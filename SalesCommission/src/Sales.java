import java.util.Scanner;

public class Sales
{
	private String salesPerson;
	private Scanner input;
	
	//constructor takes sales persons name
	public Sales(String name)
	{
		salesPerson = name;
	}
	
	public void setSalesPerson(String person)
	{
		salesPerson = person;
	}
	
	public String getSalesPerson()
	{
		return salesPerson;
	}
	
	public void displayMessage()
	{
		System.out.printf("Welcome to the pay report for %s\n", salesPerson);
	}
	
	public void determinePay()
	{
		input = new Scanner(System.in);
		
		double total = 0.0;
		final double commission = 0.09;
		double sale = 0.0;
		
		System.out.print("Enter sale item amount or -1 to quit: ");
		sale = input.nextDouble();
		
		//determine total sales until user enters sentinel value
		while (sale!=-1)
		{
			total = total + sale;
			
			System.out.print("Enter sale item amount or -1 to quit: ");
			sale = input.nextDouble();
		}
		
		total = 200 + total * commission;
		System.out.printf("Hello %s, your pay will be $%.2f", salesPerson, total);
	}
}
